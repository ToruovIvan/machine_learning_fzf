#include <iostream>
#include <ANN.h>
using namespace std;
using namespace ANN;

int main()
{
	setlocale(LC_ALL, "Russian");

	cout << "hello ANN!" << endl;
	cout << GetTestString().c_str() << endl;


	vector<vector<float>> input;
	vector<vector<float>> out;

	if (LoadData("../Debug/xor.data", input, out))
	{
		cout << "������� ���������" << endl;
	}
	vector<size_t> Configuration = vector<size_t>(); //������� ������������ ����

	Configuration.push_back(2);//1-�� ������� ����  ���� � ����� ���������
	Configuration.push_back(10);
	Configuration.push_back(10);
	Configuration.push_back(1);//1 �������� ����

	ANeuralNetwork::ActivationType activation_type = ANeuralNetwork::POSITIVE_SYGMOID;

	shared_ptr<ANeuralNetwork> Ann = CreateNeuralNetwork(Configuration, activation_type, 1);

	BackPropTraining(Ann, input, out, 10000, 0.1, 0.1, true); // ������� ���� ������� ��������� ��������������� ������

	cout << Ann->GetType() << endl;

	Ann->Save("../Debug/Ann.txt");
	return 0;
}