#include <iostream>
#include <ANN.h>
using namespace std;
using namespace ANN;

int main()
{
	setlocale(LC_ALL, "Russian");
	cout << "hello ANN!" << endl;
	cout << GetTestString().c_str() << endl;
	vector<vector<float>> input, output, res;

	vector<size_t> Configuration = vector<size_t>(); //������� ������������ ����

	Configuration.push_back(2);//1-�� ������� ����  ���� � ����� ���������
	Configuration.push_back(10);
	Configuration.push_back(10);
	Configuration.push_back(1);//1 �������� ����

	ANeuralNetwork::ActivationType activation_type = ANeuralNetwork::POSITIVE_SYGMOID;

	shared_ptr<ANeuralNetwork> Ann = CreateNeuralNetwork(Configuration, activation_type, 1);

	Ann->Load("../Debug/Ann.txt");

	cout << Ann->GetType() << endl;

	LoadData("../Debug/xor.data", input, output);

	vector<vector<float>> error = vector<vector<float>>(input.size());

	for (int i = 0; i < input.size(); i++)
	{
		error[i] = Ann->Predict(input[i]);
	}

	for (int i = 0; i < input.size(); i++) {
		for (int j = 0; j < output[i].size(); j++) {
			for (int k = 0; k < input[i].size(); k++)
			{
				cout << input[i][k] << "\t";
			}
			cout << output[i][0] << "\t" << error[i][0];
		}
		cout << endl;
	}

	return 0;
}