#include <iostream>
#include "Visualisation.h"
#include <PolynomialManager.h>
#include <FeatureExtraction.h>
#include <ComplexMoments.h>

using namespace std;
using namespace fe;

int main()
{
	int max_order = 12, diameter = 100;
	int squareside = 100;
	cout << fe::GetTestString().c_str() << endl;
	setlocale(LC_ALL, "ru");
	cout << "������� �������� = " << max_order << endl;
	cin >> max_order;

	cv::Mat src;
	shared_ptr<PolynomialManager> basis = CreatePolynomialManager();
	basis->InitBasis(max_order, diameter);

	ShowPolynomials("Window", basis->GetBasis());

	cout << basis->GetType() << endl;


	src = cv::imread("../Debug/numbers.png", cv::ImreadModes::IMREAD_GRAYSCALE);
	cv::imshow("Numbers", src);

	shared_ptr<IBlobProcessor> handler = CreateBlobProcessor();

	vector<cv::Mat> blobs = handler->DetectBlobs(src);

	vector<cv::Mat> normBlobs = handler->NormalizeBlobs(blobs, squareside);

	cout << handler->GetType() << endl;

	for (auto& blob : normBlobs)
	{
		ComplexMoments decomposeblob = basis->Decompose(blob);
		cv::Mat recovery = basis->Recovery(decomposeblob);
		ShowBlobDecomposition("recovery", blob, recovery);
	}

	//cv::threshold(src, dst, threshold_value, 255, threshold_type);

	//dst.convertTo(dst_bin, CV_8UC1, MAX_LEVEL, MIDDLE_LEVEL);
	//cv::cvtColor(dst, dst_bin, CV_BGR2GRAY);
	//if (dst_bin.type() != CV_8UC1) cout << "Uncorrect format!" << endl;

	//Show64FC1Mat("binary", dst);
	//region = handler->DetectBlobs(dst_bin);
	//region = handler->NormalizeBlobs(region, squareside);

	//cout << handler->GetType();
	////� region �������� �����
	////region - ������� �������

	//for (auto& blob : region)
	//{
	//	ComplexMoments number = basis->Decompose(blob);
	//	cv::Mat recovery = basis->Recovery(number);
	//	ShowBlobDecomposition("output", blob, recovery);
	//}

	cv::waitKey();
	return 0;
}